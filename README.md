# Terraform Exercise   

### Installation  
To install the infrastructure, run the following commands in a terminal:  
    
    $ git clone https://gitlab.com/sela-1090/students/yakirlevii/terraform.git  
    
    $ cd terraform_exercise  

    $ cd infrastructure  

Open a PowerShell/CMD terminal in the terraform directory and run the following commands:  
    
    $ terraform init  

* you can run terraform validate to validate the syntax of Terraform files

```
$ terraform plan

$ terraform apply 
```


Once you complete the application process, you will receive a public IP address as output. Please insert this IP address into the URL and press enter. Then, you can proceed to input your data. If you wish to view the data later, simply visit http://<public_ip_address>/data/<name> and press enter.

GOODLUCK! 



